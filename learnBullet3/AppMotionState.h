﻿#ifndef APPMOTIONSTATE_H
#define APPMOTIONSTATE_H

#include <bullet/btBulletCollisionCommon.h>

class AppMotionState: public btDefaultMotionState
{
public:
    AppMotionState(const btTransform &transform) : btDefaultMotionState(transform) {}

    void GetWorldTransform(btScalar* transform)
    {
        btTransform trans;
        getWorldTransform(trans);
        trans.getOpenGLMatrix(transform);
    }

};

#endif // APPMOTIONSTATE_H
