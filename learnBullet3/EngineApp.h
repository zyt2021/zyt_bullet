﻿#ifndef ENGINEAPP_H
#define ENGINEAPP_H

#include <App.h>
#include <AppMotionState.h>
#include <iostream>
class EngineApp : public App
{
public:
    virtual void initPhysics() override;
    virtual void closePhysics() override;

    void creatObjects();
};

#endif // ENGINEAPP_H
