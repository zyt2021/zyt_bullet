﻿#include "App.h"

App::App()
    :_camPos(10.f,5.f,0.f),
      _camTarget(0,0,0),
      _upVector(0.f,1.f,0.f),
      _nearPlane(1.f),
      _farPlane(1000.f),
      m_pBroadphase(0),
      m_pCollisionConfiguration(0),
      m_pDispatcher(0),
      m_pSolver(0),
      m_pWorld(0)
      //m_pMotionState(0)
{

}

App::~App()
{
    closePhysics();
}

void App::init()
{

    //为我们的环境，漫反射，镜面反射和位置创建一些float
    GLfloat ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f }; // dark grey
    GLfloat diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // white
    GLfloat specular[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // white
    GLfloat position[] = { 5.0f, 10.0f, 1.0f, 0.0f };

    //为LIGHT0设置环境光、漫反射光、高光和位置
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT0, GL_POSITION, position);

    glEnable(GL_LIGHTING); // enables lighting
    glEnable(GL_LIGHT0); // enables the 0th light
    glEnable(GL_COLOR_MATERIAL); // 启用照明时为材质着色

    // 通过材质启用镜面反射照明
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
    glMateriali(GL_FRONT, GL_SHININESS, 15);

    // 启用平滑着色
    glShadeModel(GL_SMOOTH);

    // enable depth testing to be 'less than'
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //设置背景颜色
    glClearColor(0.6,0.65,0.85,0);

    initPhysics();

    m_pDebugDrawer = new MyDrawer();
    m_pDebugDrawer->setDebugMode(1);

    m_pWorld->setDebugDrawer(m_pDebugDrawer);

    m_pDebugDrawer->ToggleDebugFlag(btIDebugDraw::DBG_DrawAabb);
    //m_pDebugDrawer->ToggleDebugFlag(btIDebugDraw::DBG_DrawWireframe);

}

void App::keyboard(uint8_t key, int x, int y)
{

}

void App::keyboardUp(uint8_t key, int x, int y)
{

}

void App::reshape(int w, int h)
{
    _screenHeight = h;
    _screenWidth  = w;

    glViewport(0,0,w,h);

    updateCam();
}

void App::display()
{

}

void App::idle()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // get the time since the last iteration
    float dt = m_clock.getTimeMilliseconds();
    // reset the clock to 0
    m_clock.reset();
    // update the scene (convert ms to s)
    updateScene(dt / 1000.0f);

    updateCam();

    // render the scene
    renderScene();

    glutSwapBuffers();
}

void App::renderScene()
{
    // 创建一个16个浮点数的数组（表示4x4矩阵）
    btScalar transform[16];
//    if (m_pMotionState)
//    {
//        // 从世界中获取运动状态
//        m_pMotionState->GetWorldTransform(transform);
//        // 喂给绘图显示
//        drawBox(transform, btVector3(1,1,1), btVector3(1.0f,0.2f,0.2f));
//    }

    for(GameObjects::iterator i=m_objects.begin();i != m_objects.end();++i)
    {
        GameObject* obj = *i;

        obj->getTransform(transform);

        drawShape(transform, obj->getPShape(), obj->getColor());
    }

    m_pWorld->debugDrawWorld();
}

void App::updateScene(float dt)
{
    // check if the world object exists
    if (m_pWorld) {
        // step the simulation through time. This is called
        // every update and the amount of elasped time was
        // determined back in ::Idle() by our clock object.
        m_pWorld->stepSimulation(dt);
    }
}

void App::initPhysics()
{

}

void App::closePhysics()
{

}

void App::updateCam()
{
    if(_screenHeight == 0 && _screenWidth == 0)
    {
        return;
    }

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    float aspectRatio = _screenWidth / (float)_screenHeight;

    glFrustum (-aspectRatio * _nearPlane, aspectRatio * _nearPlane, -_nearPlane, _nearPlane, _nearPlane, _farPlane);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    gluLookAt(_camPos[0], _camPos[1], _camPos[2], _camTarget[0], _camTarget[1], _camTarget[2], _upVector.getX(), _upVector.getY(), _upVector.getZ());
}

void App::drawBox(const btVector3 &halfSize)
{
    // push the transform onto the stack
    glPushMatrix();


    float halfWidth = halfSize.x();
    float halfHeight = halfSize.y();
    float halfDepth = halfSize.z();

    // set the object's color

    // create the vertex positions
    btVector3 vertices[8]={
        btVector3(halfWidth,halfHeight,halfDepth),
        btVector3(-halfWidth,halfHeight,halfDepth),
        btVector3(halfWidth,-halfHeight,halfDepth),
        btVector3(-halfWidth,-halfHeight,halfDepth),
        btVector3(halfWidth,halfHeight,-halfDepth),
        btVector3(-halfWidth,halfHeight,-halfDepth),
        btVector3(halfWidth,-halfHeight,-halfDepth),
        btVector3(-halfWidth,-halfHeight,-halfDepth)};

    // create the indexes for each triangle, using the
    // vertices above. Make it static so we don't waste
    // processing time recreating it over and over again
    static int indices[36] = {
        0,1,2,
        3,2,1,
        4,0,6,
        6,0,2,
        5,1,4,
        4,1,0,
        7,3,1,
        7,1,5,
        5,4,7,
        7,4,6,
        7,2,3,
        7,6,2};

    // start processing vertices as triangles
    glBegin (GL_TRIANGLES);

    // increment the loop by 3 each time since we create a
    // triangle with 3 vertices at a time.

    for (int i = 0; i < 36; i += 3) {
        // get the three vertices for the triangle based
        // on the index values set above
        // use const references so we don't copy the object
        // (a good rule of thumb is to never allocate/deallocate
        // memory during *every* render/update call. This should
        // only happen sporadically)
        const btVector3 &vert1 = vertices[indices[i]];
        const btVector3 &vert2 = vertices[indices[i+1]];
        const btVector3 &vert3 = vertices[indices[i+2]];

        // create a normal that is perpendicular to the
        // face (use the cross product)
        btVector3 normal = (vert3-vert1).cross(vert2-vert1);
        normal.normalize ();

        // set the normal for the subsequent vertices
        glNormal3f(normal.getX(),normal.getY(),normal.getZ());

        // create the vertices
        glVertex3f (vert1.x(), vert1.y(), vert1.z());
        glVertex3f (vert2.x(), vert2.y(), vert2.z());
        glVertex3f (vert3.x(), vert3.y(), vert3.z());
    }

    // stop processing vertices
    glEnd();
}

void App::drawShape(btScalar *transform, const btCollisionShape *pShape, const btVector3 &color)
{
    glColor3f(color.x(),color.y(),color.z());

    glPushMatrix();
    glMultMatrixf(transform);

    switch (pShape->getShapeType())
    {
    case BOX_SHAPE_PROXYTYPE:
    {
        const btBoxShape* box = static_cast<const btBoxShape*>(pShape);

        btVector3 halfSize = box->getHalfExtentsWithMargin();

        drawBox(halfSize);
        break;
    }
    default:
        break;
    }

    glPopMatrix();
}

GameObject *App::creatGameObject(btCollisionShape *pShape, const float &mass, const btVector3 &color, const btVector3 &initPos, const btQuaternion &initRotation)
{
    GameObject* obj = new GameObject(pShape, mass,color,initPos,initRotation);
    m_objects.push_back(obj);

    if(m_pWorld)
    {
        m_pWorld->addRigidBody(obj->getPBody());
    }
    return obj;
}
