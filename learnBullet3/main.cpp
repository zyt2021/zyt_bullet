﻿#include <AppCallBack.h>
#include <EngineApp.h>

int main(int argc, char *argv[])
{
    EngineApp app;
    return realMain(argc, argv, 1024, 768, "Introduction to Game Physics with Bullet Physics and OpenGL", &app);

}
