﻿#ifndef APP_H
#define APP_H
#include <stdint.h>
#include <windows.h>
#include <gl/GL.h>
#include <gl/freeglut.h>
#include <bullet/LinearMath/btVector3.h>
#include <bullet/BulletDynamics/Dynamics/btDynamicsWorld.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <AppMotionState.h>
#include <GameObject.h>
#include <vector>
#include <Drawer.h>
typedef std::vector<GameObject*> GameObjects;

class App
{
public:
    App();
    ~App();

    void init();

    virtual void keyboard(uint8_t key, int x, int y);
    virtual void keyboardUp(uint8_t key, int x, int y);
    virtual void reshape(int w, int h);
    virtual void display();
    virtual void idle();

    // rendering. Can be overrideen by derived classes
    virtual void renderScene();

    // scene updating. Can be overridden by derived classes
    virtual void updateScene(float dt);

    virtual void initPhysics();
    virtual void closePhysics();


    void updateCam();
    void drawBox(const btVector3 &halfSize);
    void drawShape(btScalar* transform, const btCollisionShape* pShape, const btVector3 &color);
    GameObject* creatGameObject(btCollisionShape* pShape,
                                const float &mass,
                                const btVector3 &color = btVector3(1.0f,1.0f,1.0f),
                                const btVector3 &initPos = btVector3(0,0,0),
                                const btQuaternion &initRotation = btQuaternion(0,0,1,1)
                                );
protected:
     btVector3 _camPos;
     btVector3 _camTarget;
     float _nearPlane;
     float _farPlane;
     btVector3 _upVector;

     int _screenWidth;
     int _screenHeight;

     //bullet内核
     btBroadphaseInterface* m_pBroadphase;
     btCollisionConfiguration* m_pCollisionConfiguration;
     btCollisionDispatcher* m_pDispatcher;
     btConstraintSolver* m_pSolver;
     btDynamicsWorld* m_pWorld;

    // 运动状态
    //AppMotionState* m_pMotionState;

    // 计时器
    btClock m_clock;

    GameObjects m_objects;

    MyDrawer *m_pDebugDrawer;
};

#endif // APP_H
