﻿#ifndef APPCALLBACK_H
#define APPCALLBACK_H
#include <App.h>

static App *app;

static void keyboardCallBack(uint8_t key, int x,int y)
{
    app->keyboard(key,x,y);
}

static void keyboardUpCallBack(uint8_t key, int x,int y)
{
    app->keyboardUp(key,x,y);
}

static void idleCallBack()
{
    app->idle();
}
static void displayCallBack()
{
    app->display();
}

static void reshapeCallBack(int w, int h)
{
    app->reshape(w,h);
}
int realMain(int argc, char **argv, int width, int height, const char* title, App* pApp)
{
    app = pApp;
    glutInit(&argc, argv); //初始化glut
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);//glut显示模式
    glutInitWindowPosition(0, 0); //glut显示位置
    glutInitWindowSize(width, height); //glut宽高
    glutCreateWindow(title); //设置窗口标题
    glutSetOption (GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

    app->init();
    glutKeyboardFunc(keyboardCallBack); //键盘回调
    glutKeyboardUpFunc(keyboardUpCallBack);
    glutReshapeFunc(reshapeCallBack);//reshape回调
    glutIdleFunc(idleCallBack);//默认函数
    glutDisplayFunc(displayCallBack); //显示回调

    app->idle();

    glutMainLoop();
    return 0;
}

#endif // APPCALLBACK_H
