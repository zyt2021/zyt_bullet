﻿#include "EngineApp.h"

void EngineApp::initPhysics()
{

    //    点点约束 btPoint2PointConstraint
    //    铰链约束 btHingeConstraint
    //    滑动约束 btSliderConstraint
    //    锥形约束 btConeTwistConstraint
    //    通用的6自由度约束 btGeneric6DofConstraint
    //    接触点约束 btContactConstraint

    // 在btSequentialImpulseConstraintSolver中将碰撞信息创建成了btSolverConstraint，而它没有继承自btTypedConstraint。

    // 碰撞配置
    m_pCollisionConfiguration = new btDefaultCollisionConfiguration();
    // 事件调度器
    m_pDispatcher = new btCollisionDispatcher(m_pCollisionConfiguration);
    // Broad Phase碰撞方法
    m_pBroadphase = new btDbvtBroadphase();
    // 约束求解器
    m_pSolver = new btSequentialImpulseConstraintSolver();
    // 创建仿真世界
    m_pWorld = new btDiscreteDynamicsWorld(m_pDispatcher, m_pBroadphase, m_pSolver, m_pCollisionConfiguration);

    //std::cout<< m_pWorld->getGravity()<<std::endl;
    // 创建仿真物体
    creatObjects();
}

void EngineApp::closePhysics()
{
    delete m_pWorld;
    delete m_pSolver;
    delete m_pBroadphase;
    delete m_pDispatcher;
    delete m_pCollisionConfiguration;
}

void EngineApp::creatObjects()
{
    //   // 创建正方体形状
    //   btBoxShape* pBoxShape = new btBoxShape(btVector3(1.0f, 1.0f, 1.0f));
    //   // 正方体的初始位置
    //   btTransform transform;
    //   transform.setIdentity();
    //   transform.setOrigin(btVector3(0.0f, 0.0f, 0.0f));
    //   // 创建运动状态
    //   m_pMotionState = new AppMotionState(transform);
    //   // 创建刚体构造信息对象，使其质量为1、运动状态和形状
    //   btRigidBody::btRigidBodyConstructionInfo rbInfo(10.0f, m_pMotionState, pBoxShape);
    //   btRigidBody* pRigidBody = new btRigidBody(rbInfo);
    //   // 告诉我们的世界，我们刚刚创造了一个新的刚性体来管理它
    //   m_pWorld->addRigidBody(pRigidBody);


    /*ADD*/	// create a ground plane
    /*ADD*/	creatGameObject(new btBoxShape(btVector3(1,10,10)), 0, btVector3(0.2f, 0.6f, 0.6f), btVector3(0.0f, 0.0f, 0.0f));
    /*ADD*/
    /*ADD*/	// create our original red box
    /*ADD*/	creatGameObject(new btBoxShape(btVector3(1,1,1)), 1.0, btVector3(1.0f, 0.2f, 0.2f), btVector3(0.0f, 10.0f, 0.0f));
    /*ADD*/
    /*ADD*/	// create a second box
    /*ADD*/	creatGameObject(new btBoxShape(btVector3(1,1,1)), 1.0, btVector3(0.0f, 0.2f, 0.8f), btVector3(0.0f, 20.0f, 1.25f));


}
