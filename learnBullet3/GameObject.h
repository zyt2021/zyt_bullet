#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <bullet/btBulletDynamicsCommon.h>
#include <AppMotionState.h>

class GameObject
{
public:
    GameObject(btCollisionShape* getPShape, float mass, const btVector3 &getColor, const btVector3 &initialPosition = btVector3(0,0,0), const btQuaternion &initialRotation = btQuaternion(0,0,1,1));
    ~GameObject();

    btCollisionShape *getPShape() const;

    btRigidBody *getPBody() const;

    AppMotionState *getPMotionState() const;

    btVector3 getColor() const;

    void getTransform(btScalar* transform);

protected:
    btCollisionShape *m_pShape;
    btRigidBody *m_pBody;
    AppMotionState  *m_pMotionState;
    btVector3       m_color;
};

#endif // GAMEOBJECT_H
