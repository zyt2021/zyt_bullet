#include "Drawer.h"

MyDrawer::MyDrawer()
{

}

void MyDrawer::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
    /*ADD*/		glBegin(GL_LINES);
    /*ADD*/		glColor3f(color.getX(), color.getY(), color.getZ());
    /*ADD*/		glVertex3f(from.getX(), from.getY(), from.getZ());
    /*ADD*/		glVertex3f(to.getX(), to.getY(), to.getZ());
    /*ADD*/		glEnd();
}

void MyDrawer::drawContactPoint(const btVector3 &pointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color)
{
    /*ADD*/		// draws a line between two contact points
    /*ADD*/		btVector3 const startPoint = pointOnB;
    /*ADD*/		btVector3 const endPoint = pointOnB + normalOnB * distance;
    /*ADD*/		drawLine( startPoint, endPoint, color );
}

void MyDrawer::ToggleDebugFlag(int flag)
{
    /*ADD*/		if (m_debugMode & flag)
    /*ADD*/			// flag is enabled, so disable it
    /*ADD*/			m_debugMode = m_debugMode & (~flag);
    /*ADD*/		else
    /*ADD*/			// flag is disabled, so enable it
    /*ADD*/			m_debugMode |= flag;
}


