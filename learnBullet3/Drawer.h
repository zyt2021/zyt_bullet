#ifndef DRAWER_H
#define DRAWER_H

#include "LinearMath/btIDebugDraw.h"
#include <Windows.h>
#include <gl/GL.h>
#include <gl/glut.h>
class MyDrawer: public btIDebugDraw
{
public:
    MyDrawer();
    void setDebugMode(int debugMode) override{m_debugMode=debugMode;}
    int getDebugMode() const override{return m_debugMode;}

    void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) override;
    void drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color) override;

    void ToggleDebugFlag(int flag);

    /*ADD*/		virtual void  reportErrorWarning(const char* warningString) override {}
    /*ADD*/		virtual void  draw3dText(const btVector3 &location,const char* textString) override {}

protected:
    int m_debugMode = 1;
};

#endif // DRAWER_H
