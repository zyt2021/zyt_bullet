#include "GameObject.h"

GameObject::GameObject(btCollisionShape *pShape, float mass, const btVector3 &color, const btVector3 &initialPosition, const btQuaternion &initialRotation)
{
    m_pShape = pShape;
    m_color  = color;

    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(initialPosition);
    transform.setRotation(initialRotation);

    m_pMotionState = new AppMotionState(transform);

    // 转动惯量
    btVector3 localInteria(0,0,0);

    // 质量
    if(mass != 0.0f)
    {
        //计算转动惯量
        pShape->calculateLocalInertia(mass, localInteria);
    }

    //创建刚体描述
    btRigidBody::btRigidBodyConstructionInfo cInfo(mass, m_pMotionState, pShape,localInteria);

    //创建一个刚体
    m_pBody = new btRigidBody(cInfo);
}

GameObject::~GameObject()
{
    delete  m_pBody;
    delete  m_pMotionState;
    delete  m_pShape;
}

btCollisionShape *GameObject::getPShape() const
{
    return m_pShape;
}

btRigidBody *GameObject::getPBody() const
{
    return m_pBody;
}

AppMotionState *GameObject::getPMotionState() const
{
    return m_pMotionState;
}

btVector3 GameObject::getColor() const
{
    return m_color;
}

void GameObject::getTransform(btScalar *transform)
{
    if(m_pMotionState)
    {
        m_pMotionState->GetWorldTransform(transform);
    }
}
